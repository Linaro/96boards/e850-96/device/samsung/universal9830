/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
//#define LOG_NDEBUG 0

#include <cutils/properties.h>

#define LOG_TAG "UNIVERSAL9830_PowerHAL"
#include <log/log.h>

#include <hardware/hardware.h>
#include <hardware/power.h>

#include "epic_manager.h"
#include "encoding_hint.h"
#include "decoding_hint.h"

#define UNUSED(expr) do { (void)(expr); } while (0)

struct universal9830_power_module {
	struct power_module base;
	pthread_mutex_t lock;
	int boostpulse_fd;
	int boostpulse_warned;
};

// EPIC APIs
static epic_manager epic_mgr;
static encoding_hint encoding;
static decoding_hint decoding;

static void power_init(struct power_module *module)
{
	ALOGI("power init\n");

	encoding.init();
	decoding.init();
}

static void power_set_interactive(struct power_module *module, int on)
{
	UNUSED(module);

	ALOGV("power_set_interactive: %d\n", on);

	/*
	 * called when screen is on/off.
	 */

	ALOGV("power_set_interactive: %d done\n", on);
}

static void universal9830_power_hint(struct power_module *module, power_hint_t hint,
		void *data)
{
	struct universal9830_power_module *universal9830 = (struct universal9830_power_module *) module;

	switch (hint) {
	case POWER_HINT_INTERACTION:
		break;
	case POWER_HINT_VSYNC:
		break;
	case POWER_HINT_VIDEO_ENCODE: {
		encoding.parse_power_hint(data);
		encoding.boosting(epic_mgr);
		break;
	}
	case POWER_HINT_VIDEO_DECODE: {
		decoding.parse_power_hint(data);
		decoding.boosting(encoding.get_instance_count());
		break;
	}
	default:
		break;
	}
}

static struct hw_module_methods_t power_module_methods = {
	.open = NULL,
};

struct universal9830_power_module HAL_MODULE_INFO_SYM = {
	.base = {
		.common = {
			.tag = HARDWARE_MODULE_TAG,
			.module_api_version = POWER_MODULE_API_VERSION_0_2,
			.hal_api_version = HARDWARE_HAL_API_VERSION,
			.id = POWER_HARDWARE_MODULE_ID,
			.name = "universal9830 Power HAL",
			.author = "The Android Open Source Project",
			.methods = &power_module_methods,
		},
		.init = power_init,
		.setInteractive = power_set_interactive,
		.powerHint = universal9830_power_hint,
	},
	.lock = PTHREAD_MUTEX_INITIALIZER,
	.boostpulse_fd = -1,
	.boostpulse_warned = 0,
};
