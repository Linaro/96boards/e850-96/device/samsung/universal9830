#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PIT_SECTOR_SIZE			512
#define PIT_MAX_PART_NUM		63					/* PIT_MAX_PART_NUM + 1 should be 16 (=4096/256) */
#define PIT_BLK_START			65536

typedef unsigned char __u8;
typedef unsigned short __le16;
typedef unsigned int __le32;

struct pit_header {
	__le32	magic;		/* to check integrity */
	__le32	count;		/* a number of partitions */
	__le32	pb_ver;		/* PIT builder version */

	__u8	reserved[PIT_SECTOR_SIZE / 2 - 12];
} __attribute__((packed));

struct pit_entry {
	__le32	id;		/* Not used, but set for tracability */
	__le32	filesys;	/* if this is assumed as flahsing sparse images */
	__le32	blkstart;	/* calculated, start lba */
	__le32	blknum;		/* block count */
	__le32	lun;		/* partition category # */
	__u8	name[16];	/* partition name */
	__u8	option[16];	/* for various features */
	__u8	info[36];	/* info, only used in host, for customer-specifics */

	__u8	reserved[PIT_SECTOR_SIZE / 2 - 88];
} __attribute__((packed));

struct pit_info {
	struct pit_header	hdr;
	struct pit_entry	pte[PIT_MAX_PART_NUM];
} __attribute__((packed));


enum pit_filesys {
	FS_TYPE_NONE		= 0,
	FS_TYPE_BASIC,
	FS_TYPE_SPARSE_EXT4	= 5,
	FS_TYPE_SPARSE_F2FS,
};
