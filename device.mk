#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_SHIPPING_API_LEVEL := 29

include $(LOCAL_PATH)/BoardConfig.mk

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts
PREBUILD_PATH_BASE := vendor/samsung_slsi/exynos9830/prebuilts/universal9830
ifneq ($(wildcard $(BOARD_PREBUILTS)),)
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
ifeq ($(BOARD_AVB_ENABLE), true)
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
endif
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))

INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILTS)/dtb.img
else
INSTALLED_KERNEL_TARGET := $(PREBUILD_PATH_BASE)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(PREBUILD_PATH_BASE)/dtbo.img
BOARD_PREBUILT_DTB := $(PREBUILD_PATH_BASE)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(PREBUILD_PATH_BASE)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel \
		device/samsung/universal9830/flashall.sh:$(PRODUCT_OUT)/flashall.sh \
		device/samsung/universal9830/flashall.bat:$(PRODUCT_OUT)/flashall.bat
endif

ifeq ($(BOARD_AVB_ENABLE), true)
# recovery mode
BOARD_INCLUDE_RECOVERY_DTBO := true
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_BOOTIMG_HEADER_VERSION := 2
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0

else
TARGET_NO_RECOVERY := true
endif


ifeq ($(BOARD_AVB_ENABLE), true)
# Enable Dynamic Partitions
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Enable system-as-root
#BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
else
# Enable system-as-root
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
endif

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
# Dynamic Partitions options
BOARD_SUPER_PARTITION_SIZE := 5997854720

# Configuration for dynamic partitions.
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 5599395840
ifeq ($(WITH_ESSI),true)
BOARD_GROUP_BASIC_PARTITION_LIST := vendor
else
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif
endif

PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel


# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on \
    audio.offload.min.duration.secs=30

# Device Manifest, Device Compatibility Matrix for Treble
DEVICE_MANIFEST_FILE := \
    device/samsung/universal9830/manifest.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
    device/samsung/universal9830/framework_compatibility_matrix.xml

DEVICE_MATRIX_FILE := \
    device/samsung/universal9830/compatibility_matrix.xml

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9830/overlay-sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9830/overlay-ufsboot
else
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9830/overlay-emmcboot
endif
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/universal9830/overlay

# Init files
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/init.exynos9830.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).rc \
	device/samsung/universal9830/conf/init.exynos9830.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc \
	device/samsung/universal9830/conf/ueventd.exynos9830.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	device/samsung/universal9830/conf/init.recovery.exynos9830.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.recovery.$(TARGET_SOC).rc \
	device/samsung/universal9830/conf/init.recovery.exynos9830.rc:root/init.recovery.$(TARGET_SOC).rc

# for off charging mode
PRODUCT_PACKAGES += \
	charger_res_images

# The universal board does not have a product partition, so skip the partition mount.
# If the product partition is added, Information related to the product partition should be deleted in the file.
PRODUCT_COPY_FILES += device/samsung/universal9830/conf/skip_mount.cfg:system/etc/init/config/skip_mount.cfg

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
# DYNAMIC_PARTITIONS case
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/fstab.universal9830.dp.sdboot:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC) \
	device/samsung/universal9830/conf/fstab.universal9830.dp.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/universal9830/conf/fstab.universal9830.dp.sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/fstab.universal9830.dp:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC) \
	device/samsung/universal9830/conf/fstab.universal9830.dp:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/universal9830/conf/fstab.universal9830.dp
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/fstab.universal9830.dp.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC) \
	device/samsung/universal9830/conf/fstab.universal9830.dp.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/universal9830/conf/fstab.universal9830.dp.emmc
endif
endif
else
# SYSTEM AS ROOT case
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/fstab.universal9830.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/universal9830/conf/fstab.exynos9830.sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/fstab.universal9830:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/universal9830/conf/fstab.exynos9830
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/conf/fstab.universal9830.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/universal9830/conf/fstab.exynos9830.emmc
endif
endif
endif

# Support devtools
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	Development
endif

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA - tlrpmb
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/09090000070100010000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/09090000070100010000000000000000.tlbin

# CBD (CP booting deamon)
CBD_USE_V2 := true

ifeq ($(BOARD_USES_EXYNOS_SENSORS_DUMMY), true)
# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)
endif

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service.$(TARGET_SOC) \

# Fastboot HAL
PRODUCT_PACKAGES += \
	fastbootd \
	android.hardware.fastboot@1.1\
	android.hardware.fastboot@1.1-impl-mock.$(TARGET_SOC) \

# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power@1.0-impl \
	android.hardware.power@1.0-service \
	power.$(TARGET_SOC)

# configStore HAL
PRODUCT_PACKAGES += \
	android.hardware.configstore@1.0-service \
	android.hardware.configstore@1.0-impl

# tetheroffload HAL
#PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.tetheroffload@1.0-service

# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@2.0-impl \
	android.hardware.thermal@2.0-service.exynos

#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service

#
# Audio HALs
#

# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := false
USE_XML_AUDIO_POLICY_CONF := 1

# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@5.0-impl \
	android.hardware.audio.effect@5.0-impl \
	android.hardware.soundtrigger@2.0-impl

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	frameworks/av/services/audiopolicy/config/hearing_aid_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/hearing_aid_audio_policy_configuration.xml \
	device/samsung/universal9830/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	device/samsung/universal9830/audio/config/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml

# Mixer Path Configuration for AudioHAL
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/universal9830/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# Calliope firmware overwrite
ifeq ($(TARGET_USE_EVT0),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/firmware/evt0/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal9830/firmware/evt0/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal9830/firmware/evt0/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/universal9830/firmware/evt0/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/universal9830/firmware/evt0/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/universal9830/firmware/evt0/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/universal9830/firmware/evt0/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/universal9830/firmware/evt0/txse1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse1.bin
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/firmware/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal9830/firmware/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal9830/firmware/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/universal9830/firmware/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/universal9830/firmware/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/universal9830/firmware/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/universal9830/firmware/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/universal9830/firmware/txse1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse1.bin
endif

# AP SE parameter overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/firmware/AP_AUDIO_SLSI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/AP_AUDIO_SLSI.bin

# VTS firmware overwrite
PRODUCT_COPY_FILES += \
        device/samsung/universal9830/firmware/vts.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/vts.bin

ifeq ($(BOARD_USE_USB_OFFLOAD), true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/audio/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	device/samsung/universal9830/audio/config/mixer_usb_default.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_default.xml \
	device/samsung/universal9830/audio/config/mixer_usb_white.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_white.xml \
	device/samsung/universal9830/audio/config/mixer_usb_gray.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_gray.xml
else
PRODUCT_PACKAGES += \
	audio.usb.default
PRODUCT_COPY_FILES += \
	frameworks/av/services/audiopolicy/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml
endif

# BT A2DP HAL Server & Default Implementations
ifeq ($(BOARD_USE_BTA2DP_OFFLOAD),true)
PRODUCT_PACKAGES += \
        android.hardware.bluetooth.a2dp@1.0-impl-exynos \
        vendor.samsung_slsi.hardware.ExynosA2DPOffload@2.0-impl
else
# PRODUCT_PACKAGES += \
        audio.a2dp.default
endif

# AudioEffectHAL library
#ifeq ($(BOARD_USE_OFFLOAD_AUDIO), true)
#ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
#PRODUCT_PACKAGES += \
	libexynospostprocbundle
#endif
#endif

# Enable AAudio MMAP/NOIRQ data path.
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# MIDI support native XML
PRODUCT_COPY_FILES += \
        frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

# SoundTriggerHAL library
ifeq ($(BOARD_USE_SOUNDTRIGGER_HAL), true)
PRODUCT_PACKAGES += \
        sound_trigger.primary.$(TARGET_SOC)
# VoiceTriggerSystem (VTS) HW test application
PRODUCT_PACKAGES_ENG += \
        vtshw-test
endif

# A-Box Service Daemon
PRODUCT_PACKAGES += main_abox

# TinyTools for Audio
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    cplay
endif

# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# Debugging utilities
PRODUCT_PACKAGES += android.hardware.dumpstate@1.0-service.exynos \
		    ionps

# for now include gralloc here. should come from hardware/samsung_slsi/exynos5
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl-2.1 \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.0-impl \
	android.hardware.drm@1.0-service \
	android.hardware.drm@1.4-service.clearkey \
	android.hardware.drm@1.4-service.widevine

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlsecdrm \
	liboemcrypto_modular

# tlwvdrm
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/00060308060501020000000000000000.tabin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00060308060501020000000000000000.tabin

# CryptoManager - tlcmdrv
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/FFFFFFFFD00000000000000000000016.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFFD00000000000000000000016.tlbin

# CryptoManager test app for eng build
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
# tlcmtest / cm_test
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/04010000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/04010000000000000000000000000000.tlbin \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/cm_test:$(TARGET_COPY_OUT_VENDOR)/app/cm_test
endif

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.1-service \
	RootPA \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic \
	tee_tlcm \
	tee_whitelist

ifeq ($(BOARD_MOBICORE_ENABLE),true)
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi500
endif
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos9830/secapp

# Camera HAL
PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service_64 \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
ifeq ($(TARGET_USE_EVT0),true)
PRODUCT_COPY_FILES += \
    device/samsung/universal9830/firmware/camera/evt0/fimc_is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/universal9830/firmware/camera/evt0/fimc_is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/universal9830/firmware/camera/evt0/setfile_2l4.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2l4.bin \
    device/samsung/universal9830/firmware/camera/evt0/setfile_3j1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3j1.bin \
    device/samsung/universal9830/firmware/camera/evt0/setfile_3m3.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3m3.bin \
    device/samsung/universal9830/firmware/camera/evt0/setfile_3p9.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3p9.bin \
    device/samsung/universal9830/firmware/camera/evt0/setfile_4ha.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_4ha.bin
else
PRODUCT_COPY_FILES += \
    device/samsung/universal9830/firmware/camera/fimc_is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/universal9830/firmware/camera/fimc_is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/universal9830/firmware/camera/setfile_2ld.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2ld.bin \
    device/samsung/universal9830/firmware/camera/setfile_3j1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3j1.bin \
    device/samsung/universal9830/firmware/camera/setfile_2la.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2la.bin \
    device/samsung/universal9830/firmware/camera/setfile_gw2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gw2.bin
endif

# Copy Camera HFD Setfiles
#PRODUCT_COPY_FILES += \
    device/samsung/universal9830/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/universal9830/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/universal9830/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/universal9830/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

# Copy Camera NFD Setfiles
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/firmware/camera/libvpl/default_configuration.flm.cfg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.flm.cfg.bin \
	device/samsung/universal9830/firmware/camera/libvpl/NPU_NFD_P4.7.tflite:$(TARGET_COPY_OUT_VENDOR)/firmware/NPU_NFD_P4.7.tflite

PRODUCT_COPY_FILES += \
	device/samsung/universal9830/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \


# SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
    ro.surface_flinger.vsync_event_phase_offset_ns=0 \
    ro.surface_flinger.vsync_sf_event_phase_offset_ns=0 \
    ro.surface_flinger.max_frame_buffer_acquired_buffers=3 \
    ro.surface_flinger.running_without_sync_framework=false \
    ro.surface_flinger.use_color_management=false\
    ro.surface_flinger.has_wide_color_display=false \
    persist.sys.sf.color_saturation=1.0 \
    debug.sf.latch_unsignaled=0 \
    debug.sf.high_fps_late_app_phase_offset_ns=0 \
    debug.sf.high_fps_late_sf_phase_offset_ns=0 \
    debug.sf.disable_backpressure=1

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=480 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1

# adoptable storage
PRODUCT_PROPERTY_OVERRIDES += ro.crypto.volume.filenames_mode=aes-256-cts
PRODUCT_PROPERTY_OVERRIDES += ro.crypto.allow_encrypt_override=true

# hw composer HAL
PRODUCT_PACKAGES += \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME) \
    android.hardware.graphics.composer@2.3-impl \
    android.hardware.graphics.composer@2.3-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

# set the vsync mode
ifeq ($(BOARD_USES_VSYNC_MODE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        vendor.hwc.exynos.vsync_mode=0
endif

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

####################################
## VIDEO
####################################
# MFC firmware
PRODUCT_COPY_FILES += \
    device/samsung/universal9830/firmware/mfc_fw_v14.0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin


# 1. Codec 2.0
ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
# default service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

DEVICE_MANIFEST_FILE += \
	device/samsung/universal9830/manifest_media_c2_default.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal9830/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
endif

# 2. OpenMAX IL
# OpenMAX IL configuration files
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/universal9830/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

PRODUCT_COPY_FILES += \
    device/samsung/universal9830/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy

PRODUCT_PACKAGES += \
	libstagefrighthw \
	libExynosOMX_Core \
	libExynosOMX_Resourcemanager \
	libOMX.Exynos.MPEG4.Decoder \
	libOMX.Exynos.AVC.Decoder \
	libOMX.Exynos.WMV.Decoder \
	libOMX.Exynos.VP8.Decoder \
	libOMX.Exynos.HEVC.Decoder \
	libOMX.Exynos.VP9.Decoder \
	libOMX.Exynos.MPEG4.Encoder \
	libOMX.Exynos.AVC.Encoder \
	libOMX.Exynos.VP8.Encoder \
	libOMX.Exynos.HEVC.Encoder \
	libOMX.Exynos.VP9.Encoder \
	libOMX.Exynos.AVC.WFD.Encoder \
	libOMX.Exynos.HEVC.WFD.Encoder
####################################

# Camera
PRODUCT_COPY_FILES += \
	device/samsung/universal9830/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

# Telephony
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml

$(warning #### [WIFI] WLAN_VENDOR = $(WLAN_VENDOR))
$(warning #### [WIFI] WLAN_CHIP = $(WLAN_CHIP))
$(warning #### [WIFI] WLAN_CHIP_TYPE = $(WLAN_CHIP_TYPE))
$(warning #### [WIFI] WIFI_NEED_CID = $(WIFI_NEED_CID))
$(warning #### [WIFI] ARGET_BOARD_PLATFORM = $(ARGET_BOARD_PLATFORM))
$(warning #### [WIFI] TARGET_BOOTLOADER_BOARD_NAME = $(TARGET_BOOTLOADER_BOARD_NAME))

# PRODUCT_COPY_FILES += device/samsung/universal9825/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf
PRODUCT_COPY_FILES += device/samsung/universal9830/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# Override heap growth limit due to high display density on device
# because this device has xxdpi screen.
# Note that this should be specified before reading XXX-dalvik-heap.mk
PRODUCT_PROPERTY_OVERRIDES +=           \
        dalvik.vm.heapgrowthlimit=256m  \
        dalvik.vm.heapminfree=2m

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

# Exynos OpenVX framework
PRODUCT_PACKAGES += \
		libexynosvision

ifeq ($(TARGET_USES_CL_KERNEL),true)
PRODUCT_PACKAGES += \
       libopenvx-opencl
endif

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# GateKeeper TA
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/07061000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/07061000000000000000000000000000.tlbin

# KeyManager/AES modules
PRODUCT_PACKAGES += \
	tlkeyman

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl

ifeq ($(BOARD_USES_KEYMASTER_VER4),true)
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0_tee-service \
	keymaster_drv

#tlkeymasterM
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/0706000000000000000000000000004d.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/0706000000000000000000000000004d.tlbin

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/TlcTeeKeymaster4
else
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-service
endif

# strongbox_keymaster_ta
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/00000000000000000000534258505859.tabin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00000000000000000000534258505859.tabin

PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0_strongbox-service

# include strongbox function VTS in case of ENG build
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	strongbox_function_vts
endif

# include strongbox attestation in case of ENG build
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	strongbox_attestation
endif

PRODUCT_PACKAGES += \
	wait_for_dual_keymaster

# include permission to notify that Strongbox is available
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.strongbox_keystore.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.strongbox_keystore.xml

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/ssp/strongbox_keymaster
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/ssp/wait_for_dual_keymaster

PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/13100000.ufs/by-name/frp

# Eden
PRODUCT_PACKAGES += \
    android.hardware.neuralnetworks@1.3-service.eden-drv \
    vendor.samsung_slsi.hardware.eden_runtime@1.0-impl \
    vendor.samsung_slsi.hardware.eden_runtime@1.0-service

PRODUCT_PROPERTY_OVERRIDES += \
    log.tag.EDEN=INFO \
    log.tag.NPUC=ERROR \
    ro.vendor.eden.devices=CPU1_GPU1_NPU2 \
    ro.vendor.eden.npu.version.path=/sys/devices/platform/npu_exynos/version

PRODUCT_COPY_FILES += \
	device/samsung/universal9830/eden/eden_kernel_64.bin:$(TARGET_COPY_OUT_VENDOR)/etc/eden/gpu/eden_kernel_64.bin

# OFI HAL
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.ofi@2.1-service \
	libofi_dal \
	libofi_kernels_cpu \
	libofi_klm_vendor \
	libofi_klm \
	libofi_rt_framework \
	libofi_rt_framework_user \
	libofi_rt_framework_user_vendor \
	libofi_service_interface \
	libofi_service_interface_vendor \
	libofi_seva \
	libofi_seva_vendor \
	libofib_rt_framework_user \
	libofib_rt_framework \
	libofib_service_interface \
	libinference_engine_vendor \
	libinference_engine \
	libofi_plugin_vendor \
	libofi_plugin \
	libenf_gc_system \
	libenf_gc_vendor \
	libenf_uv_gc_system \
	libenf_uv_gc_vendor \
	libann_gc_system \
	libann_gc_vendor

PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/hardware/ofi/2.1/default_E9X0 vendor/samsung_slsi/exynos/ofi/2.1_E9X0
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos/enn_driver

PRODUCT_COPY_FILES += \
	device/samsung/universal9830/firmware/dsp/dsp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp.bin \
	device/samsung/universal9830/firmware/dsp/dsp_iac_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm.bin \
	device/samsung/universal9830/firmware/dsp/dsp_iac_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm.bin \
	device/samsung/universal9830/firmware/dsp/dsp_ivp_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm.bin \
	device/samsung/universal9830/firmware/dsp/dsp_ivp_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm.bin \
	device/samsung/universal9830/firmware/dsp/dsp_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_do.bin \
	device/samsung/universal9830/firmware/dsp/dsp_iac_dm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm_do.bin \
	device/samsung/universal9830/firmware/dsp/dsp_iac_pm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm_do.bin \
	device/samsung/universal9830/firmware/dsp/dsp_ivp_dm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm_do.bin \
	device/samsung/universal9830/firmware/dsp/dsp_ivp_pm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm_do.bin \
	device/samsung/universal9830/firmware/dsp/dsp_dn.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_dn.bin \
	device/samsung/universal9830/firmware/dsp/dsp_iac_dm_dn.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm_dn.bin \
	device/samsung/universal9830/firmware/dsp/dsp_iac_pm_dn.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm_dn.bin \
	device/samsung/universal9830/firmware/dsp/dsp_ivp_dm_dn.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm_dn.bin \
	device/samsung/universal9830/firmware/dsp/dsp_ivp_pm_dn.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm_dn.bin \
	device/samsung/universal9830/firmware/dsp/dsp_master.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_master.bin \
	device/samsung/universal9830/firmware/dsp/dsp_reloc_rules.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_reloc_rules.bin \
	device/samsung/universal9830/firmware/dsp/dsp_gkt.xml:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_gkt.xml \
	device/samsung/universal9830/firmware/dsp/libivp.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libivp.elf \
	device/samsung/universal9830/firmware/dsp/liblog.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/liblog.elf \
	device/samsung/universal9830/firmware/dsp/libenf.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libenf.elf \
	device/samsung/universal9830/firmware/dsp/libenf_uv.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libenf_uv.elf \
	device/samsung/universal9830/firmware/dsp/libnn.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libnn.elf \
	device/samsung/universal9830/firmware/dsp/libann.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libann.elf

# geoTrans HAL
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.geoTransService@1.0-service

$(call inherit-product-if-exists, hardware/samsung_slsi/exynos9830/libGeoTrans/1.0/geotrans.mk)

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# Epic daemon
PRODUCT_SOONG_NAMESPACES += device/samsung/universal9830/power

PRODUCT_COPY_FILES += \
	device/samsung/universal9830/power/epic.json:$(TARGET_COPY_OUT_VENDOR)/etc/epic.json

PRODUCT_PACKAGES += epic libepic_helper

# Epic HIDL
SOONG_CONFIG_NAMESPACES += epic
SOONG_CONFIG_epic := vendor_hint
SOONG_CONFIG_epic_vendor_hint := true

PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.epic@1.0-impl \
	vendor.samsung_slsi.hardware.epic@1.0-service

# vulkan version information
PRODUCT_COPY_FILES += frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml
PRODUCT_COPY_FILES += frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml
PRODUCT_COPY_FILES += frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml
PRODUCT_COPY_FILES += frameworks/native/data/etc/android.software.vulkan.deqp.level-2020-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml
PRODUCT_COPY_FILES += frameworks/native/data/etc/android.software.opengles.deqp.level-2021-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.opengles.deqp.level.xml
PRODUCT_COPY_FILES += frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# AVB2.0, to tell PackageManager that the system supports Verified Boot(PackageManager.FEATURE_VERIFIED_BOOT)
ifeq ($(BOARD_AVB_ENABLE), true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml
endif

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

ifneq ($(MALI_ON_KEYSTONE),true)
ifneq ($(AGI_ON_KEYSTONE),true)
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos9830/libs
PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL_vulkan_symlink \
	whitelist
endif
endif

#vendor directory packages
PRODUCT_PACKAGES += \
	libstagefright_hdcp \
	libskia_opt

PRODUCT_PACKAGES += \
	mfc_fw.bin

ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += dssd
endif

#PRODUCT_COPY_FILES += \
    device/sample/etc/apns-full-conf.xml:system/etc/apns-conf.xml \

PRODUCT_PROPERTY_OVERRIDES += \
        keyguard.no_require_sim=true

# control_privapp_permissions
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

PRODUCT_PROPERTY_OVERRIDES += \
	vendor.light.video.status=0

# Set default power time slice
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	ro.vendor.power.timeslice=100

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos9830/exynos9830.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/v1/hal3/src/camera.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/modules/modules.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/eden/eden.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor_v2.mk)

# GMS Applications
ifeq ($(WITH_GMS),true)
GMS_ENABLE_OPTIONAL_MODULES := true
USE_GMS_STANDARD_CONFIG := true
$(call inherit-product-if-exists, vendor/partner_gms/products/gms.mk)
endif

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)

# Contacts
PRODUCT_PACKAGES += com.android.contacts_slsi.xml

#Enable protected contents on SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
	ro.surface_flinger.protected_contents=1
