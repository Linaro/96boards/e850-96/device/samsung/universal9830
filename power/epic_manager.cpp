#include "epic_manager.h"

epic_manager::epic_manager()
{
	init_function_ptr = NULL;
	free_function_ptr = NULL;
	request_function_ptr = NULL;
	acquire_function_ptr = NULL;
	release_function_ptr = NULL;
	epic_init();
}

epic_manager::~epic_manager()
{
	if (handle != NULL)
		dlclose(handle);
}

void epic_manager::epic_init()
{
	handle = dlopen("/vendor/lib64/libepic_helper.so", RTLD_NOW | RTLD_LOCAL);
	if (!handle) {
		ALOGE("%s: DLOPEN failed\n", __func__);
		return ;
	}

	(init_function_ptr) =
		(void (*)(void))dlsym(handle, "epic_init");
	(request_function_ptr) =
		(long (*)(int))dlsym(handle, "epic_alloc_request");
	(free_function_ptr) =
		(void (*)(long))dlsym(handle, "epic_free_request");
	(acquire_function_ptr) =
		(bool (*)(long))dlsym(handle, "epic_acquire");
	(release_function_ptr) =
		(bool (*)(long))dlsym(handle, "epic_release");

	if (!request_function_ptr || !free_function_ptr || !acquire_function_ptr
			|| !release_function_ptr || !init_function_ptr) {
		ALOGE("%s: DLSYM failed\n", __func__);
		return ;
	}

	init_function_ptr();
}

long epic_manager::request_function(int id)
{
	if (request_function_ptr == NULL) {
		ALOGE("%s: request_function_ptr is NULL\n", __func__);
		return NULL;
	}

	return request_function_ptr(id);
}

void epic_manager::free_function(long handle)
{
	if (free_function_ptr == NULL) {
		ALOGE("%s: free_function_ptr is NULL\n", __func__);
		return ;
	}

	free_function_ptr(handle);
}

bool epic_manager::acquire_function(long handle)
{
	if (acquire_function_ptr == NULL) {
		ALOGE("%s: acquire_function_ptr is NULL\n", __func__);
		return false;
	}

	return acquire_function_ptr(handle);
}

bool epic_manager::release_function(long handle)
{
	if (release_function_ptr == NULL) {
		ALOGE("%s: release_function_ptr is NULL\n", __func__);
		return false;
	}

	return release_function_ptr(handle);
}
