#include "encoding_hint.h"

encoding_hint::encoding_hint()
{
	instance_count = 0;
	pre_time = 0;
	cur_time = 0;
	scenario = NULL;
	time_slice = 0;
}

void encoding_hint::init()
{
	char value[PROPERTY_VALUE_MAX];

	instance_count = 0;
	property_get("ro.vendor.power.timeslice", value, "500");
	time_slice = atol(value) - MARGIN_TIME;
	pre_time = 0;
	cur_time = 0;
}

void encoding_hint::parse_power_hint(void *data)
{
	if (data == NULL)
		return;

	int tmp = *((int *)data);

	if (tmp == ENCODING_START_SIG) {
		instance_count++;
	} else if (tmp == ENCODING_END_SIG) {
		instance_count--;
	}
}

long encoding_hint::get_instance_count()
{
	return instance_count;
}

long encoding_hint::get_millis()
{
	timeval time;
	gettimeofday(&time, NULL);
	return (time.tv_sec * 1000) + (time.tv_usec / 1000);
}

void encoding_hint::boosting(epic_manager &epic)
{
	char value[PROPERTY_VALUE_MAX];

	property_get("persist.vendor.sys.camera.preview", value, "0");
	int camera_status = atoi(value);
	if (camera_status == CAMERA_IS_WORIKING)
		return ;

	cur_time = get_millis();
	if ((cur_time - pre_time) < time_slice)
		return;

	pre_time = cur_time;

	scenario = epic.request_function(ENCODING_SCENARIO);
	if (scenario == NULL) {
		return ;
	}
	epic.acquire_function(scenario);
	epic.free_function(scenario);
}
