#include "decoding_hint.h"

decoding_hint::decoding_hint()
{
	instance_count = 0;
	isVideoPlayBack = false;
	isLowDecoding = false;
}

void decoding_hint::init()
{
	instance_count = 0;
	isVideoPlayBack = false;
	isLowDecoding = false;
}

int decoding_hint::check_instance_count(int status)
{
	if (status == DECODING_START_SIG) {
		instance_count++;
		return 1;
	}

	if (status == DECODING_END_SIG) {
		instance_count--;
		return 1;
	}

	return 0;
}

void decoding_hint::parse_power_hint(void *data)
{
	if (data == NULL)
		return ;

	int tmp = *((int *)data);
	int mask = 0xffff;
	int type = (tmp >> 16) & mask;

	if (check_instance_count(tmp)) {
		return;
	} else if (type == mask) {
		isVideoPlayBack = (tmp & 0x1) ? true : false;
	} else {
		if (instance_count > 1) {
			isLowDecoding = false;
		} else {
			int level = tmp / baseValue;
			isLowDecoding = ( level <= 1) ? true : false;
		}
	}
}

void decoding_hint::boosting(int en_cnt)
{
	int num_of_instance = en_cnt + instance_count;
	if (isVideoPlayBack && isLowDecoding && (num_of_instance == 1)) {
		property_set(LIGHT_VIDEO_PROPERTY, BOOSTING_ENABLE);
	} else {
		property_set(LIGHT_VIDEO_PROPERTY, BOOSTING_DISABLE);
	}
}
