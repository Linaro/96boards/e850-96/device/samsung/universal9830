#pragma once
#include <stdlib.h>
#include <string.h>
#include <log/log.h>

#include <cutils/properties.h>

#include "epic_manager.h"

class decoding_hint {
	private:
		long instance_count;
		bool isVideoPlayBack;
		bool isLowDecoding;

		static constexpr char LIGHT_VIDEO_PROPERTY[30] = "vendor.light.video.status";
		static constexpr char BOOSTING_ENABLE[3] = "1";
		static constexpr char BOOSTING_DISABLE[3] = "0";
		static constexpr int DECODING_START_SIG = 1;
		static constexpr int DECODING_END_SIG = -1;
		static constexpr int baseValue = (1280*720);

		int check_instance_count(int status);
	public:
		decoding_hint();

		void init();
		void parse_power_hint(void *data);
		long get_instances_count();
		void boosting(int en_cnt);
};
