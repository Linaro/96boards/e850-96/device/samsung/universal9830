#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Should be uncommented after fixing vndk-sp violation is fixed.
PRODUCT_FULL_TREBLE_OVERRIDE := true

TARGET_BOARD_INFO_FILE := device/samsung/universal9830/board-info.txt

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_VARIANT := generic

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv7-a-neon
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_2ND_CPU_VARIANT := cortex-a15
TARGET_CPU_SMP := true

TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true

TARGET_DEVICE_NAME := universal9830
TARGET_SOC_NAME := exynos

OVERRIDE_RS_DRIVER := libRSDriverArm.so
BOARD_EGL_CFG := device/samsung/universal9830/conf/egl.cfg
#BOARD_USES_HGL := true
USE_OPENGL_RENDERER := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
BOARD_USES_EXYNOS5_COMMON_GRALLOC := true
BOARD_USES_EXYNOS_GRALLOC_VERSION := 3
BOARD_USES_ALIGN_RESTRICTION := true

BOARD_USES_GRALLOC_ION_SYNC := true

# Storage options
BOARD_USES_SDMMC_BOOT := false
BOARD_USES_UFS_BOOT := true
BOARD_KERNEL_CMDLINE += androidboot.selinux=enforce
BOARD_USES_VENDORIMAGE := true
TARGET_COPY_OUT_VENDOR := vendor
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 629145600
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_USE_F2FS := true
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := f2fs
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 3087007744
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11796480000
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
BOARD_FLASH_BLOCK_SIZE := 4096
BOARD_MOUNT_SDCARD_RW := true

# WIFI related definition
BOARD_WIFI_AVOID_IFACE_RESET_MAC_CHANGE := ture
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd
BOARD_WLAN_DEVICE           := bcmdhd
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/dhd/parameters/firmware_path"
#WIFI_DRIVER_MODULE_PATH     := "/lib/modules/dhd.ko"
WIFI_DRIVER_MODULE_NAME     := "dhd"
WIFI_DRIVER_MODULE_ARG      := "firmware_path=/system/vendor/etc/wifi/bcmdhd_sta.bin_b0 nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
WIFI_DRIVER_MODULE_AP_ARG   := "firmware_path=/system/vendor/etc/wifi/bcmdhd_apsta.bin nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
WIFI_DRIVER_MODULE_P2P_ARG  := "firmware_path=/system/vendor/etc/wifi/bcmdhd_sta.bin_b0 nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
#WIFI_DRIVER_FW_PATH_STA     := "/system/vendor/etc/wifi/bcmdhd_sta.bin"
#WIFI_DRIVER_FW_PATH_P2P     := "/system/vendor/etc/wifi/bcmdhd_sta.bin_b0"
#WIFI_DRIVER_FW_PATH_AP      := "/system/vendor/etc/wifi/bcmdhd_apsta.bin"
#WIFI_DRIVER_FW_PATH_MFG     := "/system/vendor/etc/wifi/bcmdhd_mfg.bin_b0"

# We do not want to overwrite WLAN configurations in universalxxxx/BoardConfig.mk file.

# Choose the vendor of WLAN for wlan_mfg and wifi.c
# 1. Broadcom
# 2. Atheros
# 3. TI
# 4. Qualcomm
WLAN_VENDOR = 1

# Choose the WLAN chipset
# broadcom: bcm4329, bcm4330, bcm4334, bcm43241, bcm4335, bcm4339, bcm4354
# atheros: ar6003x, ar6004, ar6005, ar603x
WLAN_CHIP := bcm4375

# Choose the type of WLAN chipset
# CoB type: COB, Module type: MODULE
WLAN_CHIP_TYPE := MODULE

########################
# Video Codec
########################
# 0. Default C2
BOARD_USE_DEFAULT_SERVICE := true

# 2. Exynos OMX
BOARD_USE_DMA_BUF := true
BOARD_USE_NON_CACHED_GRAPHICBUFFER := true
BOARD_USE_GSC_RGB_ENCODER := true
BOARD_USE_CSC_HW := false
BOARD_USE_S3D_SUPPORT := false
BOARD_USE_DEINTERLACING_SUPPORT := true
BOARD_USE_HEVCENC_SUPPORT := true
BOARD_USE_HEVC_HWIP := false
BOARD_USE_VP9DEC_SUPPORT := true
BOARD_USE_VP9ENC_SUPPORT := true
BOARD_USE_WFDENC_SUPPORT := true
BOARD_USE_CUSTOM_COMPONENT_SUPPORT := true
BOARD_USE_VIDEO_EXT_FOR_WFD_HDCP := true
BOARD_USE_SINGLE_PLANE_IN_DRM := true
########################

# GPU & OpenMAX dataspace support
BOARD_USES_EXYNOS_DATASPACE_FEATURE := true

#
# AUDIO & VOICE
#
BOARD_USES_GENERIC_AUDIO := false

# Primary AudioHAL Configuration
BOARD_USE_COMMON_AUDIOHAL := true
BOARD_USE_CALLIOPE_AUDIOHAL := false
BOARD_USE_AUDIOHAL := false
BOARD_USE_AUDIOHAL_COMV1 := true

# Audio Feature Configuration
BOARD_USE_OFFLOAD_AUDIO := true
BOARD_USE_OFFLOAD_EFFECT := false
BOARD_USE_USB_OFFLOAD := true
BOARD_USE_BTA2DP_OFFLOAD := false

# SoundTriggerHAL Configuration
BOARD_USE_SOUNDTRIGGER_HAL := true
BOARD_USE_SOUNDTRIGGER_HAL_MMAP := true

# CAMERA
BOARD_BACK_CAMERA_ROTATION := 90
BOARD_FRONT_CAMERA_ROTATION := 270
BOARD_SECURE_CAMERA_ROTATION := 0
ifneq ($(filter full_universal9830_evt0,$(TARGET_PRODUCT)),)
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_SAK2L4
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K3J1
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR := SENSOR_NAME_S5K3P9
BOARD_CAMERA_USES_HWFC := false
else
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_S5K2LD
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K3J1
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_S5KGW2
BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR := SENSOR_NAME_S5K2LA
BOARD_CAMERA_USES_HWFC := true
endif
BOARD_BACK_2_CAMERA_SENSOR_OPEN := false
BOARD_FRONT_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_FRONT_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING

BOARD_CAMERA_USES_DUAL_CAMERA := false
#BOARD_DUAL_CAMERA_REAR_ZOOM_MASTER := CAMERA_ID_BACK_0
#BOARD_DUAL_CAMERA_REAR_ZOOM_SLAVE := CAMERA_ID_BACK_1
#BOARD_DUAL_CAMERA_REAR_PORTRAIT_MASTER := CAMERA_ID_BACK_1
#BOARD_DUAL_CAMERA_REAR_PORTRAIT_SLAVE := CAMERA_ID_BACK_0
#BOARD_DUAL_CAMERA_FRONT_PORTRAIT_MASTER := CAMERA_ID_FRONT_0
#BOARD_DUAL_CAMERA_FRONT_PORTRAIT_SLAVE := CAMERA_ID_FRONT_1

BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_FAKE := true
#BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_ARCSOFT := false

BOARD_CAMERA_USES_3AA_DNG := true
BOARD_CAMERA_USES_SBWC := false
BOARD_CAMERA_USES_SLSI_VENDOR_TAGS := false

#BOARD_CAMERA_USES_LLS_SOLUTION := true
#BOARD_CAMERA_USES_CAMERA_SOLUTION_VDIS := true
BOARD_CAMERA_USES_SLSI_PLUGIN_V1 := true
BOARD_CAMERA_USES_EXYNOS_VPL := true
BOARD_CAMERA_USES_EFD := true
BOARD_CAMERA_USES_EXYNOS_LEC := false
#BOARD_CAMERA_BUILD_SOLUTION_SRC := true
#BOARD_CAMERA_USES_PIPE_HANDLER := true
#BOARD_CAMERA_USES_HIFI_LLS_CAPTURE := true
#BOARD_CAMERA_USES_HIFI_CAPTURE := true

# Modules
BOARD_MODULE_USES_GDC := true

# HWComposer
BOARD_HWC_VERSION := libhwc2.1
#TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := false
#BOARD_HDMI_INCAPABLE := true
TARGET_USES_HWC2 := true
HWC_SKIP_VALIDATE := true
#BOARD_USES_DISPLAYPORT := true
#BOARD_USES_EXTERNAL_DISPLAY_POWERMODE := true
BOARD_USES_EXYNOS_AFBC_FEATURE := true
BOARD_USES_VSYNC_MODE := true
#BOARD_USES_HDRUI_GLES_CONVERSION := true
#BOARD_USES_HWC_CPU_PERF_MODE := true
#BOARD_USES_DUAL_DISPLAY := true
VSYNC_EVENT_PHASE_OFFSET_NS := 0
SF_VSYNC_EVENT_PHASE_OFFSET_NS := 0

# HWCServices
BOARD_USES_HWC_SERVICES := true

# WifiDisplay
BOARD_USES_VIRTUAL_DISPLAY := true
BOARD_USES_DISABLE_COMPOSITIONTYPE_GLES := true
BOARD_USES_SECURE_ENCODER_ONLY := true

# SCALER
BOARD_USES_DEFAULT_CSC_HW_SCALER := true
BOARD_DEFAULT_CSC_HW_SCALER := 4
BOARD_USES_SCALER_M2M1SHOT := true

# RENDERENGINE
# Support SBWC formats 0: DISABLE, 1: ENABLE, 2: ENABLE + SUPPORT LOSSY FORMAT
#RENDERENGINE_SUPPORTS_SBWC_FORMAT := 1

# LIBHWJPEG
TARGET_USES_UNIVERSAL_LIBHWJPEG := true

# IMS
#BOARD_USES_IMS := true

# Device Tree
BOARD_USES_DT := true

# PLATFORM LOG
TARGET_USES_LOGD := true

# FMP
#BOARD_USES_FMP_DM_CRYPT := true
#BOARD_USES_FMP_FSCRYPTO := true
BOARD_USES_METADATA_PARTITION := true

# SELinux Platform Private policy for exynos
BOARD_PLAT_PRIVATE_SEPOLICY_DIR := device/samsung/sepolicy/private

# SELinux Platform Public policy for exynos
BOARD_PLAT_PUBLIC_SEPOLICY_DIR := device/samsung/sepolicy/public

# SELinux policies
SAMSUNG_SEPOLICY := device/samsung/sepolicy/common \
				    device/samsung/sepolicy/exynos9830 \
				    device/samsung/universal9830/sepolicy

ifeq (, $(findstring $(SAMSUNG_SEPOLICY), $(BOARD_SEPOLICY_DIRS)))
BOARD_SEPOLICY_DIRS += $(SAMSUNG_SEPOLICY)
endif

# SECCOMP Policy
BOARD_SECCOMP_POLICY = device/samsung/universal9830/seccomp_policy

#CURL
BOARD_USES_CURL := true

# Sensor HAL
BOARD_USES_EXYNOS_SENSORS_DUMMY := true

# VISION
# Exynos vision framework (EVF)
#TARGET_USES_EVF := true
# HW acceleration
#TARGET_USES_VPU_KERNEL := true
#TARGET_USES_SCORE_KERNEL := true
#TARGET_USES_CL_KERNEL := false

# exynos RIL
#TARGET_EXYNOS_RIL_SOURCE := true
#ENABLE_VENDOR_RIL_SERVICE := true
# Multi SIM(DSDS)
#SIM_COUNT := 2

# Support Multi SIM
#SUPPORT_MULTI_SIM := true

# GNSS
BOARD_USES_EXYNOS_GNSS_DUMMY := true

# HIDL memtrack
#PRODUCT_PACKAGES += \
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    memtrack.$(TARGET_BOARD_PLATFORM)

TARGET_BOARD_KERNEL_HEADERS := hardware/samsung_slsi/exynos/kernel-4.19-headers/kernel-headers

# SYSTEM SDK
BOARD_SYSTEMSDK_VERSIONS := 29
TARGET_USES_MKE2FS := true

#VNDK
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true
BOARD_VNDK_VERSION := current

SOONG_CONFIG_NAMESPACES += libacryl
SOONG_CONFIG_libacryl += default_compositor \
                         default_scaler \
                         default_blter
SOONG_CONFIG_libacryl_default_compositor := fimg2d_L16FSBWC
SOONG_CONFIG_libacryl_default_scaler := mscl_sbwc
SOONG_CONFIG_libacryl_default_blter := fimg2d_9810_blter
BOARD_USE_GIANT_MSCL := true

#Keymaster
BOARD_USES_KEYMASTER_VER4 := true

# WiFi related defines
# BOARD_WPA_SUPPLICANT_DRIVER      := NL80211
# BOARD_HOSTAPD_DRIVER             := NL80211
# BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_slsi
# BOARD_HOSTAPD_PRIVATE_LIB        := lib_driver_cmd_slsi
# BOARD_HAS_SAMSUNG_WLAN           := true
# WPA_SUPPLICANT_VERSION           := VER_0_8_X
# BOARD_WLAN_DEVICE                := slsi
# WIFI_DRIVER_MODULE_ARG           := ""
# WLAN_VENDOR                      := 8
# SCSC_WLAN_DEVICE_NAME            := leman_s620
# SCSC_MOREDUMP_BINARY             := moredump3.bin

# Bluetooth related defines
# BOARD_HAVE_BLUETOOTH := true
# BOARD_HAVE_BLUETOOTH_SLSI := true
# BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := hardware/samsung_slsi/libbt/include
# BLUEDROID_HCI_VENDOR_STATIC_LINKING := false

# Enable BT/WIFI related code changes in Android source files
# CONFIG_SAMSUNG_SCSC_WIFIBT       := true

# NS-IOT TEST SCRIPT. This should be REMOVED in ship build for security reason
#BOARD_USES_NSIOT_TESTSCRIPT := true

# H/W align restriction of MM IPs
BOARD_EXYNOS_S10B_FORMAT_ALIGN := 64

# Enable AVB2.0
BOARD_AVB_ENABLE := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x03000000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x03000000
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000
BOARD_AVB_ALGORITHM := SHA256_RSA4096
BOARD_AVB_KEY_PATH := device/samsung/universal9830/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_KEY_PATH := device/samsung/universal9830/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_ALGORITHM := SHA256_RSA4096
BOARD_AVB_RECOVERY_ROLLBACK_INDEX := 0
BOARD_AVB_RECOVERY_ROLLBACK_INDEX_LOCATION := 0

TARGET_FS_CONFIG_GEN ?= $(dir $(lastword $(MAKEFILE_LIST)))/config.fs

# DSP
SOONG_CONFIG_NAMESPACES += dsp
SOONG_CONFIG_dsp := \
        nnapi

SOONG_CONFIG_dsp_nnapi := false

# BOOT/VENDOR PATCH LEVEL FOR ROOT_OF_TRUST
BOOT_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)
VENDOR_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)

# Enable Mobicore
BOARD_MOBICORE_ENABLE := true

ifeq ($(MALI_ON_KEYSTONE),true)
# Allow deprecated BUILD_ module types to get DDK building
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
BUILD_BROKEN_MISSING_REQUIRED_MODULES := true
else ifeq ($(AGI_ON_KEYSTONE),true)
# Allow deprecated BUILD_ module types to get DDK building
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
endif

# libExynosGraphicbuffer
SOONG_CONFIG_NAMESPACES += exynosgraphicbuffer
SOONG_CONFIG_exynosgraphicbuffer := \
	gralloc_version

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),3)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := three
endif

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := four
endif

# USB (USB gadgethal)
SOONG_CONFIG_NAMESPACES += usbgadgethal
SOONG_CONFIG_usbgadgethal:= exynos_product
SOONG_CONFIG_usbgadgethal_exynos_product := default
