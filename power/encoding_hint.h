#pragma once
#include <stdlib.h>
#include <string.h>
#include <log/log.h>
#include <time.h>

#include <cutils/properties.h>

#include "epic_manager.h"

class encoding_hint {
	private:
		long scenario;
		long instance_count;
		long time_slice;
		long pre_time;
		long cur_time;


		static constexpr int ENCODING_START_SIG = 41;
		static constexpr int ENCODING_END_SIG = -1;
		static constexpr long MARGIN_TIME = 50L;
		static constexpr long ENCODING_SCENARIO = 3;
		static constexpr int CAMERA_IS_WORIKING = 2;

		long get_millis();
	public:
		encoding_hint();

		void init();
		void parse_power_hint(void *data);
		long get_instance_count();
		void boosting(epic_manager &epic);
};
