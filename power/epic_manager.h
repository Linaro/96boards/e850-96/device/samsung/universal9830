#pragma once
#include <dlfcn.h>
#include <stdlib.h>
#include <string.h>

#include <log/log.h>

class epic_manager {
	private:
		void *handle;

		void epic_init();
		void (*init_function_ptr)(void);
		long (*request_function_ptr)(int id);
		void (*free_function_ptr)(long handle);
		bool (*acquire_function_ptr)(long handle);
		bool (*release_function_ptr)(long handle);
	public:
		epic_manager();
		~epic_manager();

		long request_function(int id);
		void free_function(long handle);
		bool acquire_function(long handle);
		bool release_function(long handle);
};
