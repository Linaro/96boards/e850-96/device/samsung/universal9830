fastboot flash dtbo dtbo.img
fastboot flash boot boot.img
fastboot flash vbmeta vbmeta.img

fastboot reboot fastboot

fastboot flash vendor vendor.img -S 300M
fastboot flash system system.img -S 512M
fastboot reboot -w
