adb reboot bootloader

fastboot flash bootloader out/target/product/universal9830/bootloader.img
fastboot flash dtbo out/target/product/universal9830/dtbo.img
fastboot flash boot out/target/product/universal9830/boot.img
fastboot flash vbmeta out/target/product/universal9830/vbmeta.img
fastboot flash recovery out/target/product/universal9830/recovery.img

fastboot reboot fastboot

fastboot flash vendor out/target/product/universal9830/vendor.img
fastboot flash system out/target/product/universal9830/system.img
fastboot reboot -w
